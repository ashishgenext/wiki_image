package app.wikiimage.utils;

/**
 * Created by Ashish.Am.Singh on 17-06-2016.
 */
public class Const {

    public static String API_URL = "https://en.wikipedia.org/w/api.php?";
    public static String API_PARAMS = "action=query&prop=pageimages&format=json&piprop=thumbnail&";
    public static String STATUS_RESPONSE = "status";
    public static String JSON_RESPONSE = "json_response";
    public static String JSON_ERROR = "json_error";
    public static String BROADCAST_API = "app.wikiImage.volleyBroadcast";
    public static String NETWORK_CHANGE = "android.net.conn.CONNECTIVITY_CHANGE";


}
