package app.wikiimage.manager;

import android.content.Context;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import app.wikiimage.Application.MyApp;
import app.wikiimage.apiCalls.VolleyApiCall;
import app.wikiimage.utils.Utils;

/**
 * Created by Ashish.Am.Singh on 17-06-2016.
 */
public class AppManager {

    private Context mContext ;
    private VolleyApiCall apiCall ;

    public  AppManager(Context context){
        setContext(context);
        apiCall = new VolleyApiCall(getContext());
    }

    public Context getContext() {
        return mContext;
    }

    public void setContext(Context mContext) {
        this.mContext = mContext;
    }

    public void volleyApiRequest(int pageLimit,int thumbSize,String searchText){
            apiCall.request(pageLimit,thumbSize,searchText);
    }

    public ImageLoader getImageLoader(){

        return MyApp.getInstance().getImageLoader();
    }

    public Utils getUtil(){
        return new Utils();
    }


}
