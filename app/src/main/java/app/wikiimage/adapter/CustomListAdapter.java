package app.wikiimage.adapter;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;

import app.wikiimage.Application.MyApp;
import app.wikiimage.R;
import app.wikiimage.manager.AppManager;
import app.wikiimage.model.ImageModel;

/**
 * Created by Ashish on 18-06-2016.
 */
public class CustomListAdapter extends ArrayAdapter<ImageModel> {

    private AppManager manager;

    public CustomListAdapter(Context context, int resource ,ArrayList<ImageModel> list) {
        super(context, resource);
        setList(list);
        inflater = ( LayoutInflater )context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        setManager(new AppManager(getContext()));

    }

    public ArrayList<ImageModel> getList() {
        return list;
    }

    public void setList(ArrayList<ImageModel> list) {
        this.list = list;
    }

    public Context getContext() {
        return mContext;
    }

    public void setContext(Context mContext) {
        this.mContext = mContext;
    }

    ArrayList<ImageModel> list ;
    Context mContext ;
    private static LayoutInflater inflater=null;

    @Override
    public int getCount() {
        return getList().size();
    }

    @Override
    public ImageModel getItem(int position) {
       return getList().get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView  ;
        rowView = inflater.inflate(R.layout.single_list_row,null);

        NetworkImageView img = (NetworkImageView) rowView.findViewById(R.id.source_image);
     //   TextView pageId = (TextView) rowView.findViewById(R.id.pageId);
      //  TextView title = (TextView) rowView.findViewById(R.id.title);
       // pageId.setText(getItem(position).getPageId());
        //title.setText(getItem(position).getTitle());

        img.setDefaultImageResId(R.drawable.loading_default);
        img.setErrorImageResId(R.drawable.caution_128);
        img.setAdjustViewBounds(true);
        img.setImageUrl(getItem(position).getThumbnailPath(), getManager().getImageLoader());
        return  rowView ;

    }

    public AppManager getManager() {
        return manager;
    }

    public void setManager(AppManager manager) {
        this.manager = manager;
    }

}
