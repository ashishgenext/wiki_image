package app.wikiimage.ui;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import app.wikiimage.utils.Const;
import app.wikiimage.R;
import app.wikiimage.adapter.CustomListAdapter;
import app.wikiimage.customview.GifView;
import app.wikiimage.manager.AppManager;
import app.wikiimage.model.ImageModel;
import app.wikiimage.receiver.ApiReceiver;
import app.wikiimage.receiver.ApiReceiver.ApiResponseListener;
import app.wikiimage.receiver.NetworkChangeReceiver;
import app.wikiimage.receiver.NetworkChangeReceiver.NetworkChangeListener;

public class MainActivity extends Activity {
    private AppManager mAppManager;
    private static String TAG = "wikiImages " + MainActivity.class.getName();
    private EditText mSearchText;
    private SearchView mSearchView ;
    private ImageView mSearchImage;
    private ListView mListView;
    private RelativeLayout mPopupLayout;
    private ImageView mNoInternet;
    private GifView mGifView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        configure();

    }

    private void configure() {

        NetworkChangeReceiver.setNetworkChangeListener(mNetworkChangeListener);
        mPopupLayout = (RelativeLayout) findViewById(R.id.popup_layout);
        mNoInternet = (ImageView) findViewById(R.id.image_notify);
        mGifView = (GifView) findViewById(R.id.gifView);
        mListView = (ListView) findViewById(R.id.image_list);
        mAppManager = new AppManager(this);
        mSearchText = (EditText) findViewById(R.id.search_bar);
        mSearchText.addTextChangedListener(mTextChange);
        mSearchImage = (ImageView) findViewById(R.id.search_icon);
        if(mAppManager.getUtil().isNetworkAvailable(this)){
            mPopupLayout.setVisibility(View.GONE);
            mSearchText.setVisibility(View.VISIBLE);
            mNoInternet.setVisibility(View.GONE);
        }else{
            mPopupLayout.setVisibility(View.VISIBLE);
            mSearchText.setVisibility(View.GONE);
            mNoInternet.setVisibility(View.VISIBLE);
            mGifView.setVisibility(View.GONE);
            hideSoftKeyBoard();
        }
        ApiReceiver.setApiResponseListener(mApiResponseListener);
    }

    private CustomListAdapter listAdapter;
    ApiResponseListener mApiResponseListener = new ApiResponseListener() {
        @Override
        public void onResponseOk(Intent intent) {
            mGifView.setVisibility(View.GONE);
            mPopupLayout.setVisibility(View.GONE);
            ArrayList<ImageModel> imageModels = intent.getParcelableArrayListExtra(Const.JSON_RESPONSE);
//            int i = 0;
//            for (ImageModel model : imageModels) {
//                Log.d(TAG, "Count :" + i++);
//                Log.d(TAG, "PageID :" + model.getPageId());
//                Log.d(TAG, "ThumbPAth:" + model.getThumbnailPath());
//                Log.d(TAG, "Title :" + model.getTitle());
//            }
//
//            Log.d(TAG, "list size :" + imageModels.size());
            listAdapter = new CustomListAdapter(getApplicationContext(), R.layout.single_list_row,imageModels);
            mListView.setAdapter(listAdapter);

        }

        @Override
        public void onResponseFail(Intent intent) {
            //  Log.d(TAG,intent.getStringExtra(Const.JSON_ERROR));
        }
    };
    private Timer timer=new Timer();
    private final long DELAY = 1500;

    TextWatcher mTextChange = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(final CharSequence s, int start, int before, int count) {
            mPopupLayout.setVisibility(View.VISIBLE);
            mGifView.setVisibility(View.VISIBLE);
    }

        @Override
        public void afterTextChanged(final Editable s) {
            timer.cancel();
            timer = new Timer();
            timer.schedule(
                    new TimerTask() {
                        @Override
                        public void run() {

                            if (s.length() != 0) {
                                mAppManager.volleyApiRequest(50, 50, s.toString());
                            } else {
                                mAppManager.volleyApiRequest(0, 0, "");
                            }

                        }
                    },
                    DELAY
            );
          }
    };
    NetworkChangeListener mNetworkChangeListener = new NetworkChangeListener() {
        @Override
        public void onNetworkConnected() {
            mPopupLayout.setVisibility(View.GONE);
            mSearchText.setVisibility(View.VISIBLE);
            mNoInternet.setVisibility(View.GONE);
        }
        @Override
        public void onNetworkFailed() {
            hideSoftKeyBoard();
            mPopupLayout.setVisibility(View.VISIBLE);
            mSearchText.setVisibility(View.GONE);
            mNoInternet.setVisibility(View.VISIBLE);
            mGifView.setVisibility(View.GONE);
        }
    };

    @Override
    protected void onDestroy() {
        disableBroadcastReceiver();
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        enableBroadcastReceiver();
        super.onResume();
    }

    @Override
    protected void onPause() {
        disableBroadcastReceiver();
        super.onPause();
    }

    public void enableBroadcastReceiver(){
        ComponentName receiver = new ComponentName(this, NetworkChangeReceiver.class);
        PackageManager pm = this.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);

    }

    public void disableBroadcastReceiver(){
        ComponentName receiver = new ComponentName(this, NetworkChangeReceiver.class);
        PackageManager pm = this.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }

    public void hideSoftKeyBoard(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}
