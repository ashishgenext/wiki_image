package app.wikiimage.apiCalls;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import app.wikiimage.Application.MyApp;
import app.wikiimage.utils.Const;
import app.wikiimage.model.ImageModel;
import app.wikiimage.receiver.ApiReceiver;

/**
 * Created by Ashish.Am.Singh on 17-06-2016.
 */
public class VolleyApiCall {

    Context mContext;
   private int mPageLimit,mThumbsize;
   private String mSearchText ;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    public VolleyApiCall(Context context) {
        mContext = context;
    }

    public void request(int pageLimit,int thumbsize,String searchText) {
        mPageLimit = pageLimit;
        mThumbsize = thumbsize ;
        mSearchText = searchText ;

        if(mSearchText.equals("")){
            Intent intent = new Intent(mContext,ApiReceiver.class);
            //ArrayList<ImageModel> list = parseJson(response);
            ArrayList<ImageModel> list = new ArrayList<>();
            //list.add(new ImageModel("","",))
            intent.putExtra(Const.STATUS_RESPONSE,1);
            intent.putParcelableArrayListExtra(Const.JSON_RESPONSE,list);
            intent.setAction(Const.BROADCAST_API);
            mContext.sendBroadcast(intent);
            return;
        }

        String url = prepareURL();
      JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null, new Response.Listener<JSONObject>(){

          @Override
          public void onResponse(JSONObject response) {
              Intent intent = new Intent(mContext,ApiReceiver.class);
              ArrayList<ImageModel> list = parseJson(response);
              intent.putExtra(Const.STATUS_RESPONSE,1);
              intent.putParcelableArrayListExtra(Const.JSON_RESPONSE,list);
              intent.setAction(Const.BROADCAST_API);
              mContext.sendBroadcast(intent);

          }},new Response.ErrorListener(){

          @Override
          public void onErrorResponse(VolleyError error) {
              Intent intent = new Intent(mContext,ApiReceiver.class);
              intent.putExtra(Const.STATUS_RESPONSE,-1);
              intent.putExtra(Const.JSON_ERROR,error);
              intent.setAction(Const.BROADCAST_API);
              mContext.sendBroadcast(intent);
          }
      }
      );
        MyApp.getInstance().addToRequestQueue(jsonObjectRequest);

    }

    private ArrayList<ImageModel> parseJson(JSONObject jsonObject){
        ArrayList<ImageModel> imageList = new ArrayList<ImageModel>();
        try {
            JSONObject query = (JSONObject) jsonObject.get("query");
            JSONObject pages = (JSONObject) query.get("pages");

            Iterator<String> keys = pages.keys();

            while (keys.hasNext()){
                String key = (String)keys.next();
                String thumbnailPath = "";

                try{
                    JSONObject value = pages.getJSONObject(key);
                    String pageId = value.getString("pageid");
                    String title = value.getString("title");

                    if(value.has("thumbnail") ){
                        JSONObject thumbnail = value.getJSONObject("thumbnail");
                        thumbnailPath = thumbnail.getString("source");
                    }
                    imageList.add(new ImageModel(pageId,title,thumbnailPath));
                }catch(Exception e){
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return imageList ;
    }

    private String prepareURL(){
        String url = Const.API_URL+Const.API_PARAMS+"pithumbsize="+mThumbsize+"&pilimit="+mPageLimit+"&generator=prefixsearch&gpssearch="+Uri.encode(mSearchText);;
        return url ;
    }

}
