package app.wikiimage.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ashish on 17-06-2016.
 */
public class ImageModel implements Parcelable {

    private String pageId;
    private String title;
    private String thumbnailPath ;

    public ImageModel(String pageId, String title,String thumbnailPath) {
        setPageId(pageId);
        setThumbnailPath(thumbnailPath);
        setTitle(title);
    }
    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbnailPath() {
        return thumbnailPath;
    }

    public void setThumbnailPath(String thumbnailPath) {
        this.thumbnailPath = thumbnailPath;
    }





    protected ImageModel(Parcel in) {
        pageId = in.readString();
        title = in.readString();
        thumbnailPath = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(pageId);
        dest.writeString(title);
        dest.writeString(thumbnailPath);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ImageModel> CREATOR = new Parcelable.Creator<ImageModel>() {
        @Override
        public ImageModel createFromParcel(Parcel in) {
            return new ImageModel(in);
        }

        @Override
        public ImageModel[] newArray(int size) {
            return new ImageModel[size];
        }
    };
}

/*
public class ImageModel {

    private String pageId,title,thumbnailPath ;

    public ImageModel(String pageId, String title,String thumbnailPath) {
        setPageId(pageId);
        setThumbnailPath(thumbnailPath);
        setTitle(title);
    }
    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbnailPath() {
        return thumbnailPath;
    }

    public void setThumbnailPath(String thumbnailPath) {
        this.thumbnailPath = thumbnailPath;
    }

}
*/
