package app.wikiimage.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import app.wikiimage.manager.AppManager;

/**
 * Created by Ashish on 19-06-2016.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {

    private static final String LOG_TAG = "NetworkChangeReceiver";


    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v(LOG_TAG, "Receieved notification about network status");

        AppManager manager = new AppManager(context);


        if(manager.getUtil().isNetworkAvailable(context)){
            listener.onNetworkConnected();
        }else{
            listener.onNetworkFailed();
        }
    }



    public interface NetworkChangeListener{
        public void onNetworkConnected();
        public void onNetworkFailed();
    }
    public static NetworkChangeListener listener ;
    public static void setNetworkChangeListener(NetworkChangeListener networkChangeListener){
        listener = networkChangeListener;

    }



}