package app.wikiimage.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Ashish.Am.Singh on 17-06-2016.
 */
public class ApiReceiver extends BroadcastReceiver {

   private static ApiResponseListener apiListener ;

    @Override
    public void onReceive(Context context, Intent intent) {
        if(apiListener != null){
            int status = intent.getIntExtra("status",-1);

            if(status == 1){
                apiListener.onResponseOk(intent);
            }else{
                apiListener.onResponseFail(intent);
            }
        }
    }

    public interface ApiResponseListener{
        public void onResponseOk(Intent intent);
        public void onResponseFail(Intent intent);
    }

    public static void setApiResponseListener(ApiResponseListener listener){
        apiListener = listener;
    }
}
